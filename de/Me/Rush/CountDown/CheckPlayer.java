/*    */ package de.Me.Rush.CountDown;
/*    */ 
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.List;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.scheduler.BukkitScheduler;
/*    */ 
/*    */ public class CheckPlayer
/*    */ {
/* 12 */   public static boolean s = false;
/*    */ 
/*    */   public static void checkPlayer()
/*    */   {
/* 16 */     Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable()
/*    */     {
/*    */       public void run()
/*    */       {
/* 22 */         if (Main.status != GameManager.RESTART)
/*    */         {
/* 24 */           if (Main.blue.size() < 1)
/*    */           {
/* 26 */             Main.status = GameManager.RESTART;
/* 27 */             if (!CheckPlayer.s) {
/* 28 */               Bukkit.broadcastMessage(Main.pr + "§6Das §cRote Team §6hat gewonnen.");
/* 29 */               CheckPlayer.s = true;
/*    */             }
/* 31 */             CheckPlayer.restart();
/*    */           }
/* 33 */           if (Main.red.size() < 1)
/*    */           {
/* 35 */             Main.status = GameManager.RESTART;
/* 36 */             if (!CheckPlayer.s) {
/* 37 */               Bukkit.broadcastMessage(Main.pr + "§6Das §bBlaue Team §6hat gewonnen.");
/* 38 */               CheckPlayer.s = true;
/*    */             }
/* 40 */             CheckPlayer.restart();
/*    */           }
/*    */ 
/* 44 */           if (Bukkit.getOnlinePlayers().length < 2)
/*    */           {
/* 49 */             boolean b = false;
/* 50 */             if ((Main.status != GameManager.RESTART) && 
/* 51 */               (!b)) {
/* 52 */               b = true;
/* 53 */               Bukkit.broadcastMessage(Main.pr + "§6Zu wenig Spieler Online. §cDer Server Startet neu.");
/* 54 */               CheckPlayer.restart();
/*    */             }
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     , 0L, 20L);
/*    */   }
/*    */ 
/*    */   public static void restart()
/*    */   {
/* 66 */     Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
/*    */     {
/*    */       public void run()
/*    */       {
/* 71 */         Bukkit.shutdown();
/*    */       }
/*    */     }
/*    */     , 200L);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.CountDown.CheckPlayer
 * JD-Core Version:    0.6.0
 */