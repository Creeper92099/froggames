/*    */ package de.Me.Rush.CountDown;
/*    */ 
/*    */ import de.Me.Rush.Game.BossHealth;
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Data;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.List;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.scheduler.BukkitScheduler;
/*    */ 
/*    */ public class Game
/*    */ {
/*    */   public static void startGame()
/*    */   {
/* 15 */     Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable()
/*    */     {
/*    */       public void run()
/*    */       {
/* 20 */         if (Main.status == GameManager.GAME)
/*    */         {
/* 22 */           Main.MaxGameTime -= 1;
/*    */ 
/* 24 */           for (Player all : Bukkit.getOnlinePlayers()) {
/* 25 */             if (Main.blue.contains(all.getName())) {
/* 26 */               double d = Main.MaxGameTime;
/* 27 */               BossHealth.displayText(all, "§6Das Spiel endet in §b" + Main.MaxGameTime + " §6 Sekunden", d / 1200.0D);
/*    */             }
/* 29 */             if (Main.red.contains(all.getName())) {
/* 30 */               double d = Main.MaxGameTime;
/* 31 */               BossHealth.displayText(all, "§6Das Spiel endet in §b" + Main.MaxGameTime + " §6 Sekunden", d / 1200.0D);
/*    */             }
/*    */           }
/*    */ 
/* 35 */           if (Main.MaxGameTime == 0)
/*    */           {
/* 37 */             Bukkit.broadcastMessage(Main.pr + "Die Maximale Zeit ist um.");
/* 38 */             Bukkit.broadcastMessage(Main.pr + "Kein Team hat gewonnen.");
/*    */ 
/* 40 */             Main.status = GameManager.RESTART;
/*    */ 
/* 42 */             for (Player all : Bukkit.getOnlinePlayers())
/*    */             {
/* 44 */               BossHealth.displayText(all, "§c§LRESTART", 1.0D);
/*    */             }
/*    */ 
/* 48 */             Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
/*    */             {
/*    */               public void run()
/*    */               {
/* 53 */                 Bukkit.broadcastMessage(Main.pr + "Server Restart.");
/*    */               }
/*    */             }
/*    */             , 140L);
/*    */ 
/* 58 */             Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
/*    */             {
/*    */               public void run()
/*    */               {
/* 62 */                 for (Player all : Bukkit.getOnlinePlayers()) {
/* 63 */                   Data.sendToServer(all, "lobby", "");
/*    */                 }
/*    */ 
/* 67 */                 Bukkit.shutdown();
/*    */               }
/*    */             }
/*    */             , 200L);
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     , 0L, 20L);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.CountDown.Game
 * JD-Core Version:    0.6.0
 */