/*     */ package de.Me.Rush.CountDown;
/*     */ 
/*     */ import de.Me.Rush.Commands.Nick_Command;
/*     */ import de.Me.Rush.Game.BossHealth;
/*     */ import de.Me.Rush.Game.GameManager;
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.io.File;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Random;
/*     */ import net.minecraft.server.v1_7_R1.WorldServer;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.Sound;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ import org.bukkit.craftbukkit.v1_7_R1.CraftWorld;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.scheduler.BukkitScheduler;
/*     */ import org.kitteh.tag.TagAPI;
/*     */ 
/*     */ public class Lobby
/*     */ {
/*     */   public void onLobbyStart()
/*     */   {
/*  24 */     Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable()
/*     */     {
/*     */       public void run()
/*     */       {
/*  29 */         if (Bukkit.getOnlinePlayers().length >= 2)
/*     */         {
/*  31 */           if (Main.status == GameManager.LOBBY)
/*     */           {
/*  33 */             Main.Lobby -= 1;
/*     */ 
/*  35 */             for (Player all : Bukkit.getOnlinePlayers()) {
/*  36 */               all.setLevel(Main.Lobby);
/*     */             }
/*  38 */             if ((Main.Lobby == 120) || (Main.Lobby == 90) || (Main.Lobby == 60) || (Main.Lobby == 30) || (Main.Lobby == 10) || (Main.Lobby == 5) || 
/*  39 */               (Main.Lobby == 4) || (Main.Lobby == 3) || (Main.Lobby == 2) || (Main.Lobby == 1)) {
/*  40 */               Bukkit.broadcastMessage(Main.pr + "§6Das Spiel Startet in §b" + Main.Lobby + " §6Sekunden.");
/*  41 */               for (Player all : Bukkit.getOnlinePlayers()) {
/*  42 */                 all.playSound(all.getEyeLocation(), Sound.CLICK, 20.0F, 20.0F);
/*     */               }
/*     */             }
/*  45 */             if (Main.Lobby == 0) {
/*  46 */               Bukkit.broadcastMessage(Main.pr + "Das Spiel hat begonnen.");
/*  47 */               for (Player all : Bukkit.getOnlinePlayers()) {
/*  48 */                 all.playSound(all.getEyeLocation(), Sound.NOTE_PLING, 20.0F, 20.0F);
/*     */               }
/*  50 */               Main.status = GameManager.SCHUTZ;
/*  51 */               Schutz.startSchutz();
/*  52 */               CheckPlayer.checkPlayer();
/*  53 */               Lobby.this.teleport();
/*     */             }
/*     */ 
/*  59 */             for (Player all : Bukkit.getOnlinePlayers())
/*     */             {
/*  61 */               if (Main.blue.contains(all.getName())) {
/*  62 */                 double d = Main.Lobby;
/*  63 */                 BossHealth.displayText(all, "§6Team §bBlau §7| §6Start in: §b" + Main.Lobby + " §6Sekunden", d / 120.0D);
/*     */               }
/*  65 */               if (Main.red.contains(all.getName())) {
/*  66 */                 double d = Main.Lobby;
/*  67 */                 BossHealth.displayText(all, "§6Team §cRot §7| §6Start in: §b" + Main.Lobby + " §6Sekunden", d / 120.0D);
/*     */               }
/*  69 */               if (Main.random.contains(all.getName())) {
/*  70 */                 double d = Main.Lobby;
/*  71 */                 BossHealth.displayText(all, "§7Random §b| §6Start in: §b" + Main.Lobby + " §6Sekunden", d / 120.0D);
/*     */               }
/*  73 */               if ((!Main.red.contains(all.getName())) && (!Main.blue.contains(all.getName()))) {
/*  74 */                 double d = Main.Lobby;
/*  75 */                 BossHealth.displayText(all, "§6Wähle dein Team §7| §6Start in: §b" + Main.Lobby + " §6Sekunden", d / 120.0D);
/*     */               }
/*     */             }
/*     */           }
/*     */         } else {
/*  80 */           Main.Lobby = 120;
/*  81 */           for (Player all : Bukkit.getOnlinePlayers()) {
/*  82 */             double d = Main.Lobby;
/*  83 */             BossHealth.displayText(all, "§6Wartet auf Spieler", d / 120.0D);
/*  84 */             all.setLevel(120);
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     , 0L, 20L);
/*     */   }
/*     */ 
/*     */   public void teleport()
/*     */   {
/*  97 */     File file = new File("plugins/Rush/Location", "loc.yml");
/*  98 */     FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*     */ 
/* 100 */     for (Player all : Bukkit.getOnlinePlayers())
/*     */     {
/* 104 */       all.getInventory().clear();
/* 105 */       all.getInventory().setHelmet(null);
/* 106 */       all.getInventory().setChestplate(null);
/* 107 */       all.getInventory().setLeggings(null);
/* 108 */       all.getInventory().setBoots(null);
/*     */ 
/* 110 */       if ((Main.blue.contains(all.getName())) || (Main.red.contains(all.getName()))) {
/* 111 */         if (Main.blue.contains(all.getName())) {
/* 112 */           String world = cfg.getString("Rush.Red.world");
/* 113 */           double x = cfg.getDouble("Rush.Red.x");
/* 114 */           double y = cfg.getDouble("Rush.Red.y");
/* 115 */           double z = cfg.getDouble("Rush.Red.z");
/* 116 */           double yaw = cfg.getDouble("Rush.Red.yaw");
/* 117 */           double pitch = cfg.getDouble("Rush.Red.pitch");
/*     */ 
/* 119 */           Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/* 120 */           loc.setPitch((float)pitch);
/* 121 */           loc.setYaw((float)yaw);
/* 122 */           all.teleport(loc);
/*     */         }
/* 124 */         if (Main.red.contains(all.getName())) {
/* 125 */           String world = cfg.getString("Rush.Blue.world");
/* 126 */           double x = cfg.getDouble("Rush.Blue.x");
/* 127 */           double y = cfg.getDouble("Rush.Blue.y");
/* 128 */           double z = cfg.getDouble("Rush.Blue.z");
/* 129 */           double yaw = cfg.getDouble("Rush.Blue.yaw");
/* 130 */           double pitch = cfg.getDouble("Rush.Blue.pitch");
/*     */ 
/* 132 */           Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/* 133 */           loc.setPitch((float)pitch);
/* 134 */           loc.setYaw((float)yaw);
/* 135 */           all.teleport(loc);
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 140 */         if (Main.red.size() == Main.blue.size()) {
/* 141 */           int gn = new Random().nextInt(2);
/* 142 */           if (gn == 0)
/* 143 */             Main.blue.add(all.getName());
/* 144 */           else if (gn == 1) {
/* 145 */             Main.red.add(all.getName());
/*     */           }
/*     */         }
/* 148 */         if ((Main.red.size() > Main.blue.size()) && 
/* 149 */           (!Main.red.contains(all.getName())) && (!Main.blue.contains(all.getName()))) {
/* 150 */           Main.blue.add(all.getName());
/*     */         }
/*     */ 
/* 153 */         if ((Main.blue.size() > Main.red.size()) && 
/* 154 */           (!Main.red.contains(all.getName())) && (!Main.blue.contains(all.getName()))) {
/* 155 */           Main.red.add(all.getName());
/*     */         }
/*     */ 
/* 158 */         if (Main.blue.contains(all.getName())) {
/* 159 */           String world = cfg.getString("Rush.Red.world");
/* 160 */           double x = cfg.getDouble("Rush.Red.x");
/* 161 */           double y = cfg.getDouble("Rush.Red.y");
/* 162 */           double z = cfg.getDouble("Rush.Red.z");
/* 163 */           double yaw = cfg.getDouble("Rush.Red.yaw");
/* 164 */           double pitch = cfg.getDouble("Rush.Red.pitch");
/*     */ 
/* 166 */           Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/* 167 */           loc.setPitch((float)pitch);
/* 168 */           loc.setYaw((float)yaw);
/* 169 */           all.teleport(loc);
/*     */         }
/* 171 */         if (Main.red.contains(all.getName())) {
/* 172 */           String world = cfg.getString("Rush.Blue.world");
/* 173 */           double x = cfg.getDouble("Rush.Blue.x");
/* 174 */           double y = cfg.getDouble("Rush.Blue.y");
/* 175 */           double z = cfg.getDouble("Rush.Blue.z");
/* 176 */           double yaw = cfg.getDouble("Rush.Blue.yaw");
/* 177 */           double pitch = cfg.getDouble("Rush.Blue.pitch");
/*     */ 
/* 179 */           Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/* 180 */           loc.setPitch((float)pitch);
/* 181 */           loc.setYaw((float)yaw);
/* 182 */           all.teleport(loc);
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/* 189 */       if (Main.blue.contains(all.getName())) {
/* 190 */         all.sendMessage(Main.pr + "§6Du bist im §bBlauen Team§e.");
/* 191 */         if (!Nick_Command.nick.containsKey(all.getName())) {
/* 192 */           all.setPlayerListName("§b" + all.getName());
/* 193 */           TagAPI.refreshPlayer(all);
/*     */         } else {
/* 195 */           all.setPlayerListName("§b" + (String)Nick_Command.nick.get(all.getName()));
/*     */         }
/* 197 */         ((CraftWorld)all.getWorld()).getHandle().a("mobSpell", all.getLocation().getX(), all.getLocation().getY(), all.getLocation().getZ(), 20, 1.0D, 1.0D, 1.0D, 1.0D);
/*     */       }
/* 199 */       if (Main.red.contains(all.getName())) {
/* 200 */         all.sendMessage(Main.pr + "§6Du bist im §cRoten Team§e.");
/* 201 */         if (!Nick_Command.nick.containsKey(all.getName())) {
/* 202 */           all.setPlayerListName("§c" + all.getName());
/* 203 */           TagAPI.refreshPlayer(all);
/*     */         } else {
/* 205 */           all.setPlayerListName("§c" + (String)Nick_Command.nick.get(all.getName()));
/*     */         }
/* 207 */         ((CraftWorld)all.getWorld()).getHandle().a("mobSpell", all.getLocation().getX(), all.getLocation().getY(), all.getLocation().getZ(), 20, 1.0D, 1.0D, 1.0D, 1.0D);
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.CountDown.Lobby
 * JD-Core Version:    0.6.0
 */