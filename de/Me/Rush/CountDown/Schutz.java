/*    */ package de.Me.Rush.CountDown;
/*    */ 
/*    */ import de.Me.Rush.Game.BossHealth;
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.io.File;
/*    */ import java.util.List;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Location;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.scheduler.BukkitScheduler;
/*    */ 
/*    */ public class Schutz
/*    */ {
/*    */   public static void startSchutz()
/*    */   {
/* 20 */     Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable()
/*    */     {
/*    */       public void run()
/*    */       {
/* 25 */         if (Main.status == GameManager.SCHUTZ)
/*    */         {
/* 28 */           Main.Schutz -= 1;
/*    */ 
/* 30 */           for (Player all : Bukkit.getOnlinePlayers()) {
/* 31 */             all.setLevel(Main.Schutz);
/* 32 */             if (Main.blue.contains(all.getName())) {
/* 33 */               double d = Main.Lobby;
/* 34 */               BossHealth.displayText(all, "§6Die Schutzzeit endet in §b" + Main.Schutz + " §6 Sekunden", d / 15.0D);
/*    */             }
/* 36 */             if (Main.red.contains(all.getName())) {
/* 37 */               double d = Main.Lobby;
/* 38 */               BossHealth.displayText(all, "§6Die Schutzzeit endet in §b" + Main.Schutz + " §6 Sekunden", d / 15.0D);
/*    */             }
/* 40 */             if (all.getLocation().getY() > 0.0D)
/*    */               continue;
/* 42 */             File file = new File("plugins/Rush/Location", "loc.yml");
/*    */ 
/* 44 */             FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*    */ 
/* 48 */             if (all.getBedSpawnLocation() == null)
/*    */               continue;
/* 50 */             all.teleport(all.getBedSpawnLocation());
/*    */           }
/*    */ 
/* 59 */           if (Main.Schutz == 0) {
/* 60 */             Bukkit.broadcastMessage(Main.pr + "§bDie Schutzphase ist um.");
/* 61 */             Main.status = GameManager.GAME;
/* 62 */             Game.startGame();
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     , 0L, 20L);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.CountDown.Schutz
 * JD-Core Version:    0.6.0
 */