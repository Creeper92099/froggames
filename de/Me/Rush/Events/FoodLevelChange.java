/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.entity.EntityDamageEvent;
/*    */ import org.bukkit.event.entity.FoodLevelChangeEvent;
/*    */ 
/*    */ public class FoodLevelChange
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void on(FoodLevelChangeEvent e)
/*    */   {
/* 18 */     if (Main.status != GameManager.GAME)
/* 19 */       e.setCancelled(true);
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(EntityDamageEvent e) {
/* 25 */     if (Main.status != GameManager.GAME)
/* 26 */       e.setCancelled(true);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.FoodLevelChange
 * JD-Core Version:    0.6.0
 */