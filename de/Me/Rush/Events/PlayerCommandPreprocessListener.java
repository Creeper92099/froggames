/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.PlayerCommandPreprocessEvent;
/*    */ 
/*    */ public class PlayerCommandPreprocessListener
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event)
/*    */   {
/* 17 */     Player p = event.getPlayer();
/* 18 */     String cmd = "";
/* 19 */     if (event.getMessage().contains(" "))
/*    */     {
/* 21 */       String[] parts = event.getMessage().split(" ");
/* 22 */       cmd = parts[0];
/*    */     }
/*    */     else
/*    */     {
/* 26 */       cmd = event.getMessage();
/*    */     }
/* 28 */     if (cmd.equalsIgnoreCase("/msg"))
/*    */     {
/* 30 */       event.setCancelled(true);
/* 31 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 33 */     else if (cmd.equalsIgnoreCase("/tell"))
/*    */     {
/* 35 */       event.setCancelled(true);
/* 36 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 38 */     else if (cmd.equalsIgnoreCase("/pl"))
/*    */     {
/* 40 */       event.setCancelled(true);
/* 41 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 43 */     else if (cmd.equalsIgnoreCase("/about"))
/*    */     {
/* 45 */       event.setCancelled(true);
/* 46 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 48 */     else if (cmd.equalsIgnoreCase("/version"))
/*    */     {
/* 50 */       event.setCancelled(true);
/* 51 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 53 */     else if (cmd.equalsIgnoreCase("/me"))
/*    */     {
/* 55 */       event.setCancelled(true);
/* 56 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 58 */     else if (cmd.equalsIgnoreCase("/?"))
/*    */     {
/* 60 */       event.setCancelled(true);
/* 61 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/* 63 */     else if (cmd.equalsIgnoreCase("/help"))
/*    */     {
/* 65 */       event.setCancelled(true);
/* 66 */       p.sendMessage(ChatColor.RED + "Nope :3");
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.PlayerCommandPreprocessListener
 * JD-Core Version:    0.6.0
 */