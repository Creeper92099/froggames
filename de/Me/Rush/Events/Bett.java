/*     */ package de.Me.Rush.Events;
/*     */ 
/*     */ import de.Me.Rush.Game.GameManager;
/*     */ import de.Me.Rush.Main.Data;
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.util.List;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.block.Block;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.block.Action;
/*     */ import org.bukkit.event.entity.PlayerDeathEvent;
/*     */ import org.bukkit.event.player.PlayerInteractEvent;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ 
/*     */ public class Bett
/*     */   implements Listener
/*     */ {
/*     */   @EventHandler
/*     */   public void on(PlayerInteractEvent e)
/*     */   {
/*  26 */     Player p = e.getPlayer();
/*     */ 
/*  28 */     if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
/*     */     {
/*  30 */       if (Main.status != GameManager.LOBBY)
/*     */       {
/*  32 */         if (e.getClickedBlock().getType() == Material.BED_BLOCK) {
/*  33 */           e.setCancelled(true);
/*  34 */           p.sendMessage(Main.pr + "§6Du hast deinen Bett Spawn gesetzt.");
/*  35 */           p.setBedSpawnLocation(e.getClickedBlock().getLocation());
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @EventHandler
/*     */   public void on(PlayerDeathEvent e)
/*     */   {
/*  45 */     Player k = null;
/*  46 */     Player d = e.getEntity();
/*     */ 
/*  49 */     if (!(e.getEntity().getKiller() instanceof Player)) {
/*  50 */       if (Main.blue.contains(d.getName()))
/*     */       {
/*  52 */         if (d.getBedSpawnLocation() == null) {
/*  53 */           Data.sendToServer(k, "lobby", Main.pr + "Dein Bett war weg. Du bist raus.");
/*  54 */           Main.blue.remove(d.getName());
/*     */         }
/*     */ 
/*  57 */         e.setDeathMessage(Main.pr + "§b" + e.getEntity().getName() + "§6 ist gestorben.");
/*     */       }
/*     */ 
/*  60 */       if (Main.red.contains(d.getName()))
/*     */       {
/*  62 */         if (d.getBedSpawnLocation() == null) {
/*  63 */           Data.sendToServer(k, "lobby", Main.pr + "Dein Bett war weg. Du bist raus.");
/*  64 */           Main.red.remove(d.getName());
/*     */         }
/*     */ 
/*  67 */         e.setDeathMessage(Main.pr + "§c" + e.getEntity().getName() + "§6 ist gestorben.");
/*     */       }
/*     */ 
/*  70 */       return;
/*     */     }
/*  72 */     k = e.getEntity().getKiller();
/*     */ 
/*  77 */     if (Main.blue.contains(k.getName()))
/*     */     {
/*  79 */       e.setDeathMessage(Main.pr + "§b" + e.getEntity().getName() + "§e ist gestorben.");
/*     */     }
/*     */ 
/*  82 */     if (Main.red.contains(k.getName()))
/*     */     {
/*  84 */       e.setDeathMessage(Main.pr + "§c" + e.getEntity().getName() + "§e ist gestorben.");
/*     */     }
/*     */ 
/*  87 */     if ((d.getBedSpawnLocation() == null) && 
/*  88 */       (Main.blue.contains(d.getName()))) {
/*  89 */       Main.blue.remove(d.getName());
/*  90 */       Bukkit.broadcastMessage(Main.pr + d.getName() + " ist ausgeschieden. Grund: Er hatte kein Bett mehr.");
/*  91 */       k.setAllowFlight(true);
/*  92 */       k.setFlying(true);
/*  93 */       ItemStack tp = new ItemStack(Material.COMPASS);
/*  94 */       ItemMeta meta = tp.getItemMeta();
/*  95 */       tp.setItemMeta(meta);
/*  96 */       meta.setDisplayName("§9Teleporter");
/*  97 */       d.getInventory().setItem(0, tp);
/*     */ 
/* 100 */       for (Player all : Bukkit.getOnlinePlayers()) {
/* 101 */         all.hidePlayer(d);
/*     */       }
/*     */     }
/*     */ 
/* 105 */     if (Main.red.contains(d.getName())) {
/* 106 */       Main.red.remove(d.getName());
/* 107 */       Bukkit.broadcastMessage(Main.pr + d.getName() + " ist ausgeschieden. Grund: Er hatte kein Bett mehr.");
/* 108 */       k.setAllowFlight(true);
/* 109 */       k.setFlying(true);
/* 110 */       ItemStack tp = new ItemStack(Material.COMPASS);
/* 111 */       ItemMeta meta = tp.getItemMeta();
/* 112 */       tp.setItemMeta(meta);
/* 113 */       meta.setDisplayName("§9Teleporter");
/* 114 */       d.getInventory().setItem(0, tp);
/*     */ 
/* 116 */       for (Player all : Bukkit.getOnlinePlayers())
/* 117 */         all.hidePlayer(d);
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.Bett
 * JD-Core Version:    0.6.0
 */