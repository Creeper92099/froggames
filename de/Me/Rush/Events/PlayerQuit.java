/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.List;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.entity.CreatureSpawnEvent;
/*    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*    */ import org.bukkit.event.player.PlayerDropItemEvent;
/*    */ import org.bukkit.event.player.PlayerKickEvent;
/*    */ import org.bukkit.event.player.PlayerQuitEvent;
/*    */ 
/*    */ public class PlayerQuit
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void on(PlayerDropItemEvent e)
/*    */   {
/* 22 */     if (Main.status == GameManager.LOBBY)
/* 23 */       e.setCancelled(true);
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(InventoryClickEvent e)
/*    */   {
/* 30 */     if (Main.status == GameManager.LOBBY)
/* 31 */       e.setCancelled(true);
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(PlayerKickEvent e)
/*    */   {
/* 38 */     Player p = e.getPlayer();
/*    */ 
/* 40 */     e.setLeaveMessage(Main.pr + "§c" + p.getName() + " hat das Spiel Verlassen.");
/*    */ 
/* 42 */     if (Main.status != GameManager.LOBBY)
/*    */     {
/* 45 */       if ((!Main.red.contains(p.getName())) && (!Main.blue.contains(p.getName())))
/* 46 */         e.setLeaveMessage(Main.pr + "§c" + p.getName() + " hat das Spiel Verlassen.");
/*    */       else {
/* 48 */         e.setLeaveMessage(Main.pr + p.getName() + " ist ausgeschieden. Grund er ist geleftet.");
/*    */       }
/*    */     }
/* 51 */     if (ServerPing.canKick.contains(p.getName())) {
/* 52 */       ServerPing.canKick.remove(p.getName());
/*    */     }
/* 54 */     if (ServerPing.dontcanKick.contains(p.getName()))
/* 55 */       ServerPing.dontcanKick.remove(p.getName());
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(PlayerQuitEvent e)
/*    */   {
/* 62 */     Player p = e.getPlayer();
/*    */ 
/* 64 */     e.setQuitMessage(Main.pr + "§c" + p.getName() + " hat das Spiel Verlassen.");
/*    */ 
/* 66 */     if (Main.status != GameManager.LOBBY)
/*    */     {
/* 69 */       if ((!Main.red.contains(p.getName())) && (!Main.blue.contains(p.getName()))) {
/* 70 */         e.setQuitMessage(Main.pr + "§c" + p.getName() + " hat das Spiel Verlassen.");
/*    */       } else {
/* 72 */         e.setQuitMessage(Main.pr + p.getName() + " ist ausgeschieden. Grund er ist geleftet.");
/* 73 */         if (Main.blue.contains(p.getName())) {
/* 74 */           Main.blue.remove(p.getName());
/*    */         }
/* 76 */         if (Main.red.contains(p.getName())) {
/* 77 */           Main.red.remove(p.getName());
/*    */         }
/*    */       }
/*    */     }
/* 81 */     if (ServerPing.canKick.contains(p.getName())) {
/* 82 */       ServerPing.canKick.remove(p.getName());
/*    */     }
/* 84 */     if (ServerPing.dontcanKick.contains(p.getName()))
/* 85 */       ServerPing.dontcanKick.remove(p.getName());
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(CreatureSpawnEvent e) {
/* 91 */     e.setCancelled(true);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.PlayerQuit
 * JD-Core Version:    0.6.0
 */