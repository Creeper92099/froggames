/*     */ package de.Me.Rush.Events;
/*     */ 
/*     */ import de.Me.Rush.Game.BossHealth;
/*     */ import de.Me.Rush.Game.GameManager;
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.io.File;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.block.Block;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.block.BlockBreakEvent;
/*     */ import org.bukkit.event.block.BlockPlaceEvent;
/*     */ import org.bukkit.event.entity.PlayerDeathEvent;
/*     */ import org.bukkit.event.player.PlayerChangedWorldEvent;
/*     */ import org.bukkit.event.player.PlayerRespawnEvent;
/*     */ 
/*     */ public class Blocks
/*     */   implements Listener
/*     */ {
/*     */   @EventHandler
/*     */   public void on(PlayerRespawnEvent e)
/*     */   {
/*  27 */     BossHealth.removeText(e.getPlayer());
/*     */   }
/*  31 */   @EventHandler
/*     */   public void on(PlayerDeathEvent e) { BossHealth.removeText(e.getEntity()); } 
/*     */   @EventHandler
/*     */   public void on(PlayerChangedWorldEvent e) {
/*  35 */     BossHealth.removeText(e.getPlayer());
/*     */   }
/*     */ 
/*     */   @EventHandler
/*     */   public void on(BlockPlaceEvent e) {
/*  41 */     File file = new File("plugins/Rush/Location", "loc.yml");
/*     */ 
/*  43 */     FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*     */ 
/*  45 */     Player p = e.getPlayer();
/*     */ 
/*  47 */     if (Main.status == GameManager.LOBBY) {
/*  48 */       if (!p.isOp()) {
/*  49 */         e.setBuild(false);
/*  50 */         e.setCancelled(true);
/*     */       }
/*     */ 
/*  54 */       return;
/*     */     }
/*     */ 
/*  57 */     if (!isInside(e.getBlock().getLocation()))
/*     */     {
/*  59 */       e.setCancelled(true);
/*  60 */       p.sendMessage(Main.pr + "An diesen Blöcken kannst du nichts ändern.");
/*     */     }
/*     */   }
/*     */ 
/*     */   public boolean isInside(Location loc)
/*     */   {
/*  66 */     File file = new File("plugins/Rush/Location", "loc.yml");
/*  67 */     FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*  68 */     Location loc1 = getLocation("Rush.pos1", file, cfg);
/*  69 */     Location loc2 = getLocation("Rush.pos2", file, cfg);
/*  70 */     if (loc.getWorld() != loc1.getWorld()) {
/*  71 */       return false;
/*     */     }
/*     */ 
/*  74 */     int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
/*  75 */     int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
/*  76 */     int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
/*     */ 
/*  78 */     int maxX = Math.max(loc1.getBlockX(), loc2.getBlockY());
/*  79 */     int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
/*  80 */     int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
/*     */ 
/*  85 */     return (loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && 
/*  83 */       (loc.getBlockY() >= minY) && (loc.getBlockY() <= maxY) && 
/*  84 */       (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ);
/*     */   }
/*     */ 
/*     */   @EventHandler
/*     */   public void on(BlockBreakEvent e)
/*     */   {
/*  92 */     Player p = e.getPlayer();
/*     */ 
/*  94 */     if (Main.status == GameManager.LOBBY) {
/*  95 */       if (!p.isOp()) {
/*  96 */         e.setCancelled(true);
/*     */       }
/*  98 */       return;
/*     */     }
/*     */ 
/* 102 */     if (!isInside(e.getBlock().getLocation()))
/*     */     {
/* 104 */       if (!p.isOp()) {
/* 105 */         e.setCancelled(true);
/* 106 */         p.sendMessage(Main.pr + "An diesen Blöcken kannst du nichts ändern.");
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   public Location getLocation(String path, File file, FileConfiguration cfg)
/*     */   {
/* 114 */     String world = cfg.getString(path + ".world");
/* 115 */     double x = cfg.getDouble(path + ".x");
/* 116 */     double y = cfg.getDouble(path + ".y");
/* 117 */     double z = cfg.getDouble(path + ".z");
/* 118 */     double yaw = cfg.getDouble(path + ".yaw");
/* 119 */     double pitch = cfg.getDouble(path + ".pitch");
/*     */ 
/* 121 */     Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/* 122 */     loc.setYaw((float)yaw);
/* 123 */     loc.setPitch((float)pitch);
/*     */ 
/* 126 */     return loc;
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.Blocks
 * JD-Core Version:    0.6.0
 */