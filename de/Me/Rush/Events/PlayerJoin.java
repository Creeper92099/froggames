/*     */ package de.Me.Rush.Events;
/*     */ 
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Color;
/*     */ import org.bukkit.GameMode;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.player.PlayerJoinEvent;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/*     */ 
/*     */ public class PlayerJoin
/*     */   implements Listener
/*     */ {
/*     */   @EventHandler
/*     */   public void on(PlayerJoinEvent e)
/*     */   {
/*  31 */     Player p = e.getPlayer();
/*     */ 
/*  33 */     e.setJoinMessage(Main.pr + "§c" + p.getName() + " hat das Spiel Betreten.");
/*     */ 
/*  36 */     p.getInventory().clear();
/*  37 */     p.getInventory().setHelmet(null);
/*  38 */     p.getInventory().setChestplate(null);
/*  39 */     p.getInventory().setLeggings(null);
/*  40 */     p.getInventory().setBoots(null);
/*  41 */     p.setFlying(false);
/*  42 */     p.setAllowFlight(false);
/*  43 */     p.setHealth(20.0D);
/*  44 */     p.setFoodLevel(20);
/*  45 */     p.setLevel(0);
/*  46 */     p.setExp(0.0F);
/*  47 */     p.setBedSpawnLocation(null);
/*  48 */     p.setGameMode(GameMode.SURVIVAL);
/*     */ 
/*  50 */     ItemStack item2 = new ItemStack(Material.WOOL);
/*  51 */     ItemMeta meta2 = item2.getItemMeta();
/*  52 */     meta2.setDisplayName("§5Wähle dein Team aus");
/*  53 */     item2.setItemMeta(meta2);
/*     */ 
/*  55 */     File file = new File("plugins/Rush/Location", "loc.yml");
/*  56 */     FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*     */ 
/*  58 */     if (cfg.contains("Lobby.world"))
/*     */     {
/*  60 */       String world = cfg.getString("Lobby.world");
/*  61 */       double x = cfg.getDouble("Lobby.x");
/*  62 */       double y = cfg.getDouble("Lobby.y");
/*  63 */       double z = cfg.getDouble("Lobby.z");
/*  64 */       double yaw = cfg.getDouble("Lobby.yaw");
/*  65 */       double pitch = cfg.getDouble("Lobby.pitch");
/*     */ 
/*  67 */       Location loc = new Location(Bukkit.getWorld(world), x, y, z);
/*  68 */       loc.setYaw((float)yaw);
/*  69 */       loc.setPitch((float)pitch);
/*     */ 
/*  71 */       p.teleport(loc);
/*     */     }
/*     */     else {
/*  74 */       p.sendMessage(Main.pr + "Der LobbySpawn wurde noch nicht gesetzt.");
/*     */     }
/*  76 */     if (!cfg.contains("Rush.Blue")) {
/*  77 */       p.sendMessage(Main.pr + "Der Blaue Spawn wurde noch nicht gesetzt.");
/*     */     }
/*  79 */     if (!cfg.contains("Rush.Red")) {
/*  80 */       p.sendMessage(Main.pr + "Der Rote Spawn wurde noch nicht gesetzt.");
/*     */     }
/*  82 */     if ((!cfg.contains("Rush.pos1")) || (!cfg.contains("Rush.pos2"))) {
/*  83 */       p.sendMessage(Main.pr + "Die Region wo man bauen kann wurde nicht gesetzt.");
/*     */     }
/*     */ 
/*  86 */     p.getInventory().setItem(0, item2);
/*     */     Object meta1;
/*  87 */     if (Main.red.contains(p.getName())) {
/*  88 */       ItemStack item = new ItemStack(Material.WOOL, 1, 14);
/*  89 */       ItemMeta meta = item.getItemMeta();
/*  90 */       meta.setDisplayName("§a§lDu bist im §cRoten §a§lTeam");
/*  91 */       List ra = new ArrayList();
/*  92 */       for (String red : Main.red) {
/*  93 */         ra.add("§cMitglieder : ");
/*  94 */         ra.add("§c" + red);
/*     */       }
/*  96 */       meta.setLore(ra);
/*  97 */       item.setItemMeta(meta);
/*     */ 
/*  99 */       item.setItemMeta(meta);
/*     */ 
/* 101 */       ItemStack item1 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 102 */       meta1 = (LeatherArmorMeta)item1.getItemMeta();
/* 103 */       ((LeatherArmorMeta)meta1).setColor(Color.RED);
/* 104 */       ((LeatherArmorMeta)meta1).setDisplayName("§c§lRotes TEAM");
/* 105 */       item1.setItemMeta((ItemMeta)meta1);
/*     */ 
/* 107 */       p.getInventory().setChestplate(item1);
/*     */ 
/* 109 */       p.getInventory().setItem(0, item);
/*     */     }
/* 111 */     if (Main.blue.contains(p.getName()))
/*     */     {
/* 113 */       ItemStack item = new ItemStack(Material.WOOL, 1, 11);
/* 114 */       ItemMeta meta = item.getItemMeta();
/* 115 */       meta.setDisplayName("§a§lDu bist im §1Blauen §a§lTeam");
/* 116 */       List ra = new ArrayList();
/* 117 */       for (meta1 = Main.blue.iterator(); ((Iterator)meta1).hasNext(); ) { String blue = (String)((Iterator)meta1).next();
/* 118 */         ra.add("§bMitglieder:");
/* 119 */         ra.add("§b" + blue);
/*     */       }
/*     */ 
/* 123 */       meta.setLore(ra);
/* 124 */       item.setItemMeta(meta);
/*     */ 
/* 126 */       ItemStack item1 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 127 */       LeatherArmorMeta meta1 = (LeatherArmorMeta)item1.getItemMeta();
/* 128 */       meta1.setColor(Color.BLUE);
/* 129 */       meta1.setDisplayName("§b§lBlaues TEAM");
/* 130 */       item1.setItemMeta(meta1);
/*     */ 
/* 132 */       p.getInventory().setChestplate(item1);
/*     */ 
/* 134 */       p.getInventory().setItem(0, item);
/*     */     }
/* 136 */     if (Main.random.contains(p.getName())) {
/* 137 */       ItemStack item1 = new ItemStack(Material.WOOL, 1, 7);
/* 138 */       ItemMeta meta1 = item2.getItemMeta();
/* 139 */       meta2.setDisplayName("§7§lRandom Team");
/* 140 */       List ra = new ArrayList();
/* 141 */       meta2.setLore(ra);
/* 142 */       item2.setItemMeta(meta1);
/*     */ 
/* 144 */       ItemStack item3 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 145 */       LeatherArmorMeta meta3 = (LeatherArmorMeta)item3.getItemMeta();
/* 146 */       meta3.setDisplayName("§7§lRandom Team");
/* 147 */       meta3.setColor(Color.GRAY);
/* 148 */       item1.setItemMeta(meta3);
/*     */ 
/* 150 */       p.getInventory().setChestplate(item3);
/*     */     }
/* 152 */     if (Main.blue.contains(p.getName())) {
/* 153 */       p.setPlayerListName("§b" + p.getName());
/* 154 */       return;
/*     */     }
/* 156 */     if (Main.red.contains(p.getName())) {
/* 157 */       p.setPlayerListName("§c" + p.getName());
/* 158 */       return;
/*     */     }
/* 160 */     if (Main.random.contains(p.getName())) {
/* 161 */       p.setPlayerListName("§7" + p.getName());
/* 162 */       return;
/*     */     }
/* 164 */     p.setPlayerListName("§3" + p.getName());
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.PlayerJoin
 * JD-Core Version:    0.6.0
 */