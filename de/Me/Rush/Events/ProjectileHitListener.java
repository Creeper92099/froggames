/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Sound;
/*    */ import org.bukkit.World;
/*    */ import org.bukkit.entity.Arrow;
/*    */ import org.bukkit.entity.Projectile;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.entity.ProjectileHitEvent;
/*    */ 
/*    */ public class ProjectileHitListener
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void onProjectile(ProjectileHitEvent ev)
/*    */   {
/* 18 */     if ((ev.getEntity() instanceof Arrow))
/*    */     {
/* 20 */       Bukkit.getWorld(ev.getEntity().getWorld().getName()).playSound(ev.getEntity().getLocation(), Sound.NOTE_PLING, 1.0F, 4.0F);
/* 21 */       Bukkit.getWorld(ev.getEntity().getWorld().getName()).playSound(ev.getEntity().getLocation(), Sound.NOTE_PLING, 1.0F, 4.0F);
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.ProjectileHitListener
 * JD-Core Version:    0.6.0
 */