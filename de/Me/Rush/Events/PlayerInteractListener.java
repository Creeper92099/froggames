/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Material;
/*    */ import org.bukkit.SkullType;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.block.Action;
/*    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*    */ import org.bukkit.event.player.PlayerInteractEvent;
/*    */ import org.bukkit.inventory.Inventory;
/*    */ import org.bukkit.inventory.ItemStack;
/*    */ import org.bukkit.inventory.meta.ItemMeta;
/*    */ 
/*    */ public class PlayerInteractListener
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void CompassClick(PlayerInteractEvent e)
/*    */   {
/* 31 */     Player p = e.getPlayer();
/* 32 */     if (((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
/* 33 */       (p.getItemInHand().getType() != null) && 
/* 34 */       (p.getItemInHand().getType().equals(Material.COMPASS))) {
/* 35 */       Inventory inv = Bukkit.createInventory(null, 27, "§bTeleporter");
/* 36 */       Integer slots = Integer.valueOf(0);
/* 37 */       for (Player pw : Bukkit.getOnlinePlayers()) {
/* 38 */         ItemStack is = new ItemStack(Material.SKULL_ITEM.getId(), 1, (short)SkullType.PLAYER.ordinal());
/* 39 */         ItemMeta ism = is.getItemMeta();
/* 40 */         ism.setDisplayName(pw.getName());
/* 41 */         is.setItemMeta(ism);
/* 42 */         inv.setItem(slots.intValue(), is);
/* 43 */         slots = Integer.valueOf(slots.intValue() + 1);
/*    */ 
/* 45 */         p.openInventory(inv);
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void CompassClickSkull(InventoryClickEvent e)
/*    */   {
/* 56 */     if (e.getInventory().getName().contains("Teleporter")) {
/* 57 */       Player p = (Player)e.getWhoClicked();
/* 58 */       if (e.getCurrentItem().getType().equals(Material.SKULL_ITEM))
/*    */       {
/* 60 */         String name = e.getCurrentItem().getItemMeta().getDisplayName();
/* 61 */         if (Bukkit.getPlayerExact(name) != null) {
/* 62 */           Player clicked = Bukkit.getPlayerExact(name);
/* 63 */           p.teleport(clicked.getLocation());
/* 64 */           p.sendMessage("§bZum Spieler §6" + clicked.getName() + " §bTeleportiert !");
/* 65 */           p.closeInventory();
/*    */         } else {
/* 67 */           p.sendMessage("§bDer Spieler ist nicht Online !");
/* 68 */           p.closeInventory();
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.PlayerInteractListener
 * JD-Core Version:    0.6.0
 */