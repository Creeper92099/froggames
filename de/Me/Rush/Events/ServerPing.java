/*    */ package de.Me.Rush.Events;
/*    */ 
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Data;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import java.util.Random;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.PlayerLoginEvent;
/*    */ import org.bukkit.event.server.ServerListPingEvent;
/*    */ 
/*    */ public class ServerPing
/*    */   implements Listener
/*    */ {
/* 21 */   public static List<String> dontcanKick = new ArrayList();
/* 22 */   public static List<String> canKick = new ArrayList();
/*    */ 
/*    */   @EventHandler
/*    */   public void on(ServerListPingEvent e) {
/* 27 */     e.setMaxPlayers(Main.maxPlayer);
/*    */ 
/* 29 */     if (Main.status == GameManager.LOBBY) {
/* 30 */       e.setMotd("§aLobby");
/*    */     }
/* 32 */     if (Main.status == GameManager.SCHUTZ) {
/* 33 */       e.setMotd("§eSchutzphase");
/*    */     }
/* 35 */     if (Main.status == GameManager.GAME) {
/* 36 */       e.setMotd("§4INGAME");
/*    */     }
/* 38 */     if (Main.status == GameManager.RESTART)
/* 39 */       e.setMotd("§cRESTART");
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(PlayerLoginEvent e)
/*    */   {
/* 49 */     Player p = e.getPlayer();
/*    */ 
/* 51 */     if (Bukkit.getOnlinePlayers().length >= Main.maxPlayer)
/*    */     {
/* 53 */       if (Main.status != GameManager.LOBBY)
/*    */       {
/* 55 */         e.disallow(null, "§eINGAME");
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   public void premiumKick()
/*    */   {
/* 65 */     String ran = (String)canKick.get(new Random().nextInt(canKick.size()));
/*    */ 
/* 67 */     Player ps = Bukkit.getPlayer(ran);
/*    */ 
/* 69 */     Data.sendToServer(ps, "lobby", "Du wurdest von einem Premium Mitglieg gekickt.");
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Events.ServerPing
 * JD-Core Version:    0.6.0
 */