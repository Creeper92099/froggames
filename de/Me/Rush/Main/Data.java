/*    */ package de.Me.Rush.Main;
/*    */ 
/*    */ import java.io.ByteArrayOutputStream;
/*    */ import java.io.DataOutputStream;
/*    */ import java.io.IOException;
/*    */ import org.bukkit.Location;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ 
/*    */ public class Data
/*    */ {
/*    */   public static Plugin plugin;
/*    */   public static int server;
/* 15 */   public static String lobbyserver = "lobby";
/*    */ 
/*    */   public static void sendToServer(Player p, String server, String message)
/*    */   {
/* 19 */     for (int i = 0; i < 81; i++) {
/* 20 */       p.sendMessage("");
/*    */     }
/* 22 */     p.sendMessage(message);
/*    */ 
/* 24 */     Location location = p.getLocation();
/* 25 */     float yaw = location.getYaw();
/* 26 */     float o = yaw + 180.0F;
/* 27 */     if (o > 360.0F) {
/* 28 */       yaw -= 360.0F;
/*    */     }
/* 30 */     location.setYaw(yaw);
/* 31 */     p.teleport(location);
/*    */ 
/* 33 */     ByteArrayOutputStream bs = new ByteArrayOutputStream();
/* 34 */     DataOutputStream out = new DataOutputStream(bs);
/*    */     try
/*    */     {
/* 37 */       out.writeUTF("Connect");
/* 38 */       out.writeUTF(server);
/*    */     } catch (IOException localIOException) {
/*    */     }
/* 41 */     p.sendPluginMessage(plugin, "BungeeCord", bs.toByteArray());
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Main.Data
 * JD-Core Version:    0.6.0
 */