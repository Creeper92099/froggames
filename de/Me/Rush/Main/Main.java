/*     */ package de.Me.Rush.Main;
/*     */ 
/*     */ import de.Me.Rush.Commands.Nick_Command;
/*     */ import de.Me.Rush.Commands.Rush_Command;
/*     */ import de.Me.Rush.CountDown.Lobby;
/*     */ import de.Me.Rush.Events.Bett;
/*     */ import de.Me.Rush.Events.Blocks;
/*     */ import de.Me.Rush.Events.FoodLevelChange;
/*     */ import de.Me.Rush.Events.PlayerCommandPreprocessListener;
/*     */ import de.Me.Rush.Events.PlayerJoin;
/*     */ import de.Me.Rush.Events.PlayerQuit;
/*     */ import de.Me.Rush.Events.ProjectileHitListener;
/*     */ import de.Me.Rush.Events.ServerPing;
/*     */ import de.Me.Rush.Game.GameManager;
/*     */ import de.Me.Rush.Team.TeamChat;
/*     */ import de.Me.Rush.Teams.TeamSelector;
/*     */ import java.io.File;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import net.minecraft.server.v1_7_R1.WorldServer;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.GameMode;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.World;
/*     */ import org.bukkit.command.PluginCommand;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ import org.bukkit.craftbukkit.v1_7_R1.CraftWorld;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ import org.bukkit.plugin.PluginManager;
/*     */ import org.bukkit.plugin.java.JavaPlugin;
/*     */ 
/*     */ public class Main extends JavaPlugin
/*     */ {
/*     */   public static GameManager status;
/*  38 */   public static String pr = "§b[§6Rush§b] §b";
/*     */ 
/*  40 */   public static List<String> red = new ArrayList();
/*  41 */   public static List<String> blue = new ArrayList();
/*  42 */   public static List<String> random = new ArrayList();
/*     */ 
/*  44 */   public static int minPlayer = 2;
/*  45 */   public static int maxPlayer = 8;
/*     */ 
/*  47 */   public static int Lobby = 120;
/*  48 */   public static int Schutz = 15;
/*  49 */   public static int MaxGameTime = 1200;
/*  50 */   public static int Restart = 20;
/*     */   private static Main m;
/*     */ 
/*     */   public void onEnable()
/*     */   {
/*  57 */     System.out.println("=======================================================");
/*  58 */     System.out.println("|                                                     |");
/*  59 */     System.out.println("|             sRush - a Bukkit plugin                 |");
/*  60 */     System.out.println("=======================================================");
/*  61 */     m = this;
/*  62 */     setup();
/*     */ 
/*  64 */     for (Player all : Bukkit.getOnlinePlayers()) {
/*  65 */       all.setLevel(0);
/*  66 */       all.setExp(0.0F);
/*  67 */       all.getInventory().clear();
/*  68 */       all.getInventory().setHelmet(null);
/*  69 */       all.getInventory().setChestplate(null);
/*  70 */       all.getInventory().setLeggings(null);
/*  71 */       all.getInventory().setBoots(null);
/*  72 */       all.setHealth(20.0D);
/*  73 */       all.setFoodLevel(20);
/*  74 */       all.setAllowFlight(false);
/*  75 */       all.setFlying(false);
/*  76 */       all.setGameMode(GameMode.SURVIVAL);
/*  77 */       all.setPlayerListName("§3" + all.getName());
/*  78 */       all.setBedSpawnLocation(null);
/*  79 */       ItemStack item = new ItemStack(Material.WOOL);
/*  80 */       ItemMeta meta = item.getItemMeta();
/*  81 */       meta.setDisplayName("§5Wähle dein Team aus");
/*  82 */       item.setItemMeta(meta);
/*  83 */       all.getInventory().setItem(0, item);
/*  84 */       ((CraftWorld)all.getWorld()).getHandle().a("mobSpell", all.getLocation().getX(), all.getLocation().getY(), all.getLocation().getZ(), 20, 1.0D, 1.0D, 1.0D, 1.0D);
/*     */ 
/*  86 */       File file = new File("plugins/Rush/Location", "loc.yml");
/*  87 */       FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*     */ 
/*  89 */       all.teleport(new Blocks().getLocation("Lobby", file, cfg));
/*     */     }
/*     */   }
/*     */ 
/*     */   public void onDisable()
/*     */   {
/*     */   }
/*     */ 
/*     */   public static Main getInstance()
/*     */   {
/*  99 */     return m;
/*     */   }
/*     */   public void setup() {
/* 102 */     for (World w : Bukkit.getWorlds()) {
/* 103 */       w.setAutoSave(false);
/*     */     }
/*     */ 
/* 106 */     status = GameManager.LOBBY;
/* 107 */     red.clear();
/* 108 */     blue.clear();
/* 109 */     Lobby l = new Lobby();
/* 110 */     l.onLobbyStart();
/* 111 */     getCommand("Rush").setExecutor(new Rush_Command());
/* 112 */     getCommand("Nick").setExecutor(new Nick_Command());
/* 113 */     Bukkit.getPluginManager().registerEvents(new Nick_Command(), this);
/* 114 */     Bukkit.getPluginManager().registerEvents(new TeamSelector(), this);
/* 115 */     Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
/* 116 */     Bukkit.getPluginManager().registerEvents(new ServerPing(), this);
/* 117 */     Bukkit.getPluginManager().registerEvents(new PlayerQuit(), this);
/* 118 */     Bukkit.getPluginManager().registerEvents(new Blocks(), this);
/* 119 */     Bukkit.getPluginManager().registerEvents(new FoodLevelChange(), this);
/* 120 */     Bukkit.getPluginManager().registerEvents(new Bett(), this);
/* 121 */     Bukkit.getPluginManager().registerEvents(new TeamChat(), this);
/* 122 */     Bukkit.getPluginManager().registerEvents(new ProjectileHitListener(), this);
/* 123 */     Bukkit.getPluginManager().registerEvents(new PlayerCommandPreprocessListener(), this);
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Main.Main
 * JD-Core Version:    0.6.0
 */