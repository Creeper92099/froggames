/*    */ package de.Me.Rush.Team;
/*    */ 
/*    */ import de.Me.Rush.Commands.Nick_Command;
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.HashMap;
/*    */ import java.util.List;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.AsyncPlayerChatEvent;
/*    */ 
/*    */ public class TeamChat
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void on(AsyncPlayerChatEvent e)
/*    */   {
/* 19 */     Player p = e.getPlayer();
/*    */ 
/* 21 */     if (Main.status != GameManager.LOBBY)
/*    */     {
/* 23 */       if (e.getMessage().startsWith("!")) {
/* 24 */         if (!Nick_Command.nick.containsKey(p.getName()))
/* 25 */           e.setFormat("§e|§aGlobal§e| §b" + p.getName() + " §d: §e" + e.getMessage().substring(1, e.getMessage().length()));
/*    */         else
/* 27 */           e.setFormat("§e|§aGlobal§e| §b" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage().substring(1, e.getMessage().length()));
/*    */       }
/*    */       else {
/* 30 */         e.setCancelled(true);
/* 31 */         if (Main.blue.contains(p.getName())) {
/* 32 */           for (String s : Main.blue) {
/* 33 */             if (Nick_Command.nick.containsKey(p.getName())) {
/* 34 */               if (Main.blue.contains(p.getName())) {
/* 35 */                 Bukkit.getPlayer(s).sendMessage("§e| §bTeamChat§e | §b" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 36 */                 return;
/*    */               }
/* 38 */               if (Main.red.contains(p.getName())) {
/* 39 */                 Bukkit.getPlayer(s).sendMessage("§e| §cTeamChat§e | §c" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 40 */                 return;
/*    */               }
/*    */             } else {
/* 43 */               if (Main.blue.contains(p.getName())) {
/* 44 */                 Bukkit.getPlayer(s).sendMessage("§e| §bTeamChat§e | §b" + p.getName() + " §d: §e" + e.getMessage());
/* 45 */                 return;
/*    */               }
/* 47 */               if (Main.red.contains(p.getName())) {
/* 48 */                 Bukkit.getPlayer(s).sendMessage("§e| §cTeamChat§e | §c" + p.getName() + " §d: §e" + e.getMessage());
/* 49 */                 return;
/*    */               }
/*    */             }
/*    */           }
/*    */         }
/* 54 */         if (Main.red.contains(p.getName())) {
/* 55 */           for (String s : Main.red) {
/* 56 */             if (Nick_Command.nick.containsKey(p.getName())) {
/* 57 */               if (Main.blue.contains(p.getName())) {
/* 58 */                 Bukkit.getPlayer(s).sendMessage("§e| §bTeamChat§e | §b" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 59 */                 return;
/*    */               }
/* 61 */               if (Main.red.contains(p.getName())) {
/* 62 */                 Bukkit.getPlayer(s).sendMessage("§e| §cTeamChat§e | §c" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 63 */                 return;
/*    */               }
/*    */             }
/*    */             else {
/* 67 */               if (Main.blue.contains(p.getName())) {
/* 68 */                 Bukkit.getPlayer(s).sendMessage("§e| §bTeamChat§e | §b" + p.getName() + " §d: §e" + e.getMessage());
/* 69 */                 return;
/*    */               }
/* 71 */               if (Main.red.contains(p.getName())) {
/* 72 */                 Bukkit.getPlayer(s).sendMessage("§e| §cTeamChat§e | §c" + p.getName() + " §d: §e" + e.getMessage());
/* 73 */                 return;
/*    */               }
/*    */             }
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     else
/*    */     {
/* 82 */       e.setCancelled(true);
/* 83 */       if (!Nick_Command.nick.containsKey(p.getName())) {
/* 84 */         if (Main.blue.contains(p.getName())) {
/* 85 */           Bukkit.broadcastMessage("§b" + p.getName() + " §d: §e" + e.getMessage());
/* 86 */           return;
/*    */         }
/* 88 */         if (Main.red.contains(p.getName())) {
/* 89 */           Bukkit.broadcastMessage("§c" + p.getName() + " §d: §e" + e.getMessage());
/* 90 */           return;
/*    */         }
/* 92 */         if (Main.random.contains(p.getName())) {
/* 93 */           Bukkit.broadcastMessage("§7" + p.getName() + " §d: §e" + e.getMessage());
/* 94 */           return;
/*    */         }
/* 96 */         Bukkit.broadcastMessage("§3" + p.getName() + " §d: §e" + e.getMessage());
/*    */       }
/*    */       else {
/* 99 */         if (Main.blue.contains(p.getName())) {
/* 100 */           Bukkit.broadcastMessage("§b" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 101 */           return;
/*    */         }
/* 103 */         if (Main.red.contains(p.getName())) {
/* 104 */           Bukkit.broadcastMessage("§c" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 105 */           return;
/*    */         }
/* 107 */         if (Main.random.contains(p.getName())) {
/* 108 */           Bukkit.broadcastMessage("§7" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/* 109 */           return;
/*    */         }
/* 111 */         Bukkit.broadcastMessage("§3" + (String)Nick_Command.nick.get(p.getName()) + " §d: §e" + e.getMessage());
/*    */       }
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Team.TeamChat
 * JD-Core Version:    0.6.0
 */