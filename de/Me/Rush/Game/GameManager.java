/*    */ package de.Me.Rush.Game;
/*    */ 
/*    */ public enum GameManager
/*    */ {
/*  7 */   LOBBY(0), 
/*  8 */   SCHUTZ(1), 
/*  9 */   GAME(2), 
/* 10 */   RESTART(3);
/*    */ 
/*    */   int i;
/*    */ 
/* 15 */   private GameManager(int i) { this.i = i; }
/*    */ 
/*    */   public int getStatus()
/*    */   {
/* 19 */     return this.i;
/*    */   }
/*    */ 
/*    */   public void setStatus()
/*    */   {
/* 24 */     de.Me.Rush.Main.Main.status = this;
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Game.GameManager
 * JD-Core Version:    0.6.0
 */