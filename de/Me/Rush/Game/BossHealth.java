/*     */ package de.Me.Rush.Game;
/*     */ 
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.lang.reflect.Field;
/*     */ import java.util.ArrayList;
/*     */ import net.minecraft.server.v1_7_R1.DataWatcher;
/*     */ import net.minecraft.server.v1_7_R1.EntityPlayer;
/*     */ import net.minecraft.server.v1_7_R1.Packet;
/*     */ import net.minecraft.server.v1_7_R1.PacketPlayInClientCommand;
/*     */ import net.minecraft.server.v1_7_R1.PacketPlayOutEntityDestroy;
/*     */ import net.minecraft.server.v1_7_R1.PacketPlayOutEntityMetadata;
/*     */ import net.minecraft.server.v1_7_R1.PacketPlayOutSpawnEntityLiving;
/*     */ import net.minecraft.server.v1_7_R1.PlayerConnection;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
/*     */ import org.bukkit.entity.EntityType;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.scheduler.BukkitScheduler;
/*     */ 
/*     */ public abstract class BossHealth
/*     */ {
/*     */   private static final int ENTITY_ID = 1234;
/*  25 */   private static final ArrayList<Player> hasHealthBar = new ArrayList();
/*     */ 
/*     */   private static void sendPacket(Player player, Packet packet) {
/*  28 */     EntityPlayer entityPlayer = ((CraftPlayer)player).getHandle();
/*  29 */     entityPlayer.playerConnection.sendPacket(packet);
/*     */   }
/*     */ 
/*     */   private static PacketPlayOutSpawnEntityLiving getMobPacket(String text, Location loc)
/*     */   {
/*  35 */     PacketPlayOutSpawnEntityLiving mobPacket = new PacketPlayOutSpawnEntityLiving();
/*     */     try
/*     */     {
/*  41 */       Field a = mobPacket.getClass().getDeclaredField("a");
/*  42 */       Field b = mobPacket.getClass().getDeclaredField("b");
/*  43 */       Field c = mobPacket.getClass().getDeclaredField("c");
/*  44 */       Field d = mobPacket.getClass().getDeclaredField("d");
/*  45 */       Field e = mobPacket.getClass().getDeclaredField("e");
/*  46 */       Field f = mobPacket.getClass().getDeclaredField("f");
/*  47 */       Field g = mobPacket.getClass().getDeclaredField("g");
/*  48 */       Field h = mobPacket.getClass().getDeclaredField("h");
/*  49 */       Field i = mobPacket.getClass().getDeclaredField("i");
/*  50 */       Field j = mobPacket.getClass().getDeclaredField("j");
/*  51 */       Field k = mobPacket.getClass().getDeclaredField("k");
/*     */ 
/*  53 */       a.setAccessible(true);
/*  54 */       b.setAccessible(true);
/*  55 */       c.setAccessible(true);
/*  56 */       d.setAccessible(true);
/*  57 */       e.setAccessible(true);
/*  58 */       f.setAccessible(true);
/*  59 */       g.setAccessible(true);
/*  60 */       h.setAccessible(true);
/*  61 */       i.setAccessible(true);
/*  62 */       j.setAccessible(true);
/*  63 */       k.setAccessible(true);
/*     */       try
/*     */       {
/*  66 */         a.set(mobPacket, Integer.valueOf(1234));
/*  67 */         b.set(mobPacket, Byte.valueOf((byte)EntityType.ENDER_DRAGON.getTypeId()));
/*  68 */         c.set(mobPacket, Integer.valueOf((int)Math.floor(loc.getBlockX() * 32.0D)));
/*  69 */         d.set(mobPacket, Integer.valueOf((int)Math.floor(15000.0D)));
/*  70 */         e.set(mobPacket, Integer.valueOf((int)Math.floor(loc.getBlockZ() * 32.0D)));
/*  71 */         f.set(mobPacket, Byte.valueOf(0));
/*  72 */         g.set(mobPacket, Byte.valueOf(0));
/*  73 */         h.set(mobPacket, Byte.valueOf(0));
/*  74 */         i.set(mobPacket, Byte.valueOf(0));
/*  75 */         j.set(mobPacket, Byte.valueOf(0));
/*  76 */         k.set(mobPacket, Byte.valueOf(0));
/*     */       }
/*     */       catch (IllegalArgumentException er) {
/*  79 */         er.printStackTrace();
/*     */       } catch (IllegalAccessException er) {
/*  81 */         er.printStackTrace();
/*     */       }
/*     */ 
/*     */     }
/*     */     catch (NoSuchFieldException e1)
/*     */     {
/*  87 */       e1.printStackTrace();
/*     */     }
/*     */ 
/*  91 */     DataWatcher watcher = getWatcher(text, 200);
/*     */     try
/*     */     {
/*  95 */       Field t = PacketPlayOutSpawnEntityLiving.class.getDeclaredField("l");
/*  96 */       t.setAccessible(true);
/*  97 */       t.set(mobPacket, watcher);
/*     */     } catch (Exception e) {
/*  99 */       e.printStackTrace();
/*     */     }
/*     */ 
/* 102 */     return mobPacket;
/*     */   }
/*     */ 
/*     */   private static PacketPlayOutEntityDestroy getDestroyEntityPacket() {
/* 106 */     PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy();
/*     */     try {
/* 108 */       Field a = packet.getClass().getDeclaredField("a");
/* 109 */       a.setAccessible(true);
/* 110 */       a.set(packet, new int[] { 1234 });
/*     */     }
/*     */     catch (IllegalArgumentException e) {
/* 113 */       e.printStackTrace();
/*     */     }
/*     */     catch (IllegalAccessException e) {
/* 116 */       e.printStackTrace();
/*     */     }
/*     */     catch (SecurityException e1)
/*     */     {
/* 120 */       e1.printStackTrace();
/*     */     }
/*     */     catch (NoSuchFieldException e1) {
/* 123 */       e1.printStackTrace();
/*     */     }
/* 125 */     return packet;
/*     */   }
/*     */ 
/*     */   private static PacketPlayOutEntityMetadata getMetadataPacket(DataWatcher watcher) {
/* 129 */     PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata();
/*     */     try {
/* 131 */       Field a = metaPacket.getClass().getDeclaredField("a");
/* 132 */       a.setAccessible(true);
/* 133 */       a.set(metaPacket, Integer.valueOf(1234));
/*     */     }
/*     */     catch (IllegalArgumentException e) {
/* 136 */       e.printStackTrace();
/*     */     }
/*     */     catch (IllegalAccessException e) {
/* 139 */       e.printStackTrace();
/*     */     }
/*     */     catch (SecurityException e1)
/*     */     {
/* 143 */       e1.printStackTrace();
/*     */     }
/*     */     catch (NoSuchFieldException e1) {
/* 146 */       e1.printStackTrace();
/*     */     }
/*     */     try
/*     */     {
/* 150 */       Field b = PacketPlayOutEntityMetadata.class.getDeclaredField("b");
/* 151 */       b.setAccessible(true);
/* 152 */       b.set(metaPacket, watcher.c());
/*     */     } catch (Exception e) {
/* 154 */       e.printStackTrace();
/*     */     }
/* 156 */     return metaPacket;
/*     */   }
/*     */ 
/*     */   private static PacketPlayInClientCommand getRespawnPacket()
/*     */   {
/* 161 */     PacketPlayInClientCommand packet = new PacketPlayInClientCommand();
/*     */     try {
/* 163 */       Field a = packet.getClass().getDeclaredField("a");
/* 164 */       a.setAccessible(true);
/* 165 */       a.set(packet, Integer.valueOf(1));
/*     */     }
/*     */     catch (IllegalArgumentException e) {
/* 168 */       e.printStackTrace();
/*     */     }
/*     */     catch (IllegalAccessException e) {
/* 171 */       e.printStackTrace();
/*     */     }
/*     */     catch (SecurityException e1)
/*     */     {
/* 175 */       e1.printStackTrace();
/*     */     }
/*     */     catch (NoSuchFieldException e1) {
/* 178 */       e1.printStackTrace();
/*     */     }
/* 180 */     return packet;
/*     */   }
/*     */ 
/*     */   private static DataWatcher getWatcher(String text, int health) {
/* 184 */     DataWatcher watcher = new DataWatcher(null);
/* 185 */     watcher.a(0, Byte.valueOf(32));
/* 186 */     watcher.a(6, Float.valueOf(health));
/* 187 */     watcher.a(10, text);
/* 188 */     watcher.a(11, Byte.valueOf(1));
/*     */ 
/* 190 */     return watcher;
/*     */   }
/*     */ 
/*     */   public static void displayText(Player player, String text, double percent) {
/* 194 */     if (!hasHealthBar.contains(player)) {
/* 195 */       PacketPlayOutSpawnEntityLiving mobPacket = getMobPacket(text, player.getLocation());
/* 196 */       sendPacket(player, mobPacket);
/* 197 */       hasHealthBar.add(player);
/*     */     }
/* 199 */     Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable(text, percent, player)
/*     */     {
/*     */       public void run()
/*     */       {
/* 203 */         DataWatcher watcher = BossHealth.access$0(BossHealth.this, (int)(this.val$percent * 200.0D));
/* 204 */         PacketPlayOutEntityMetadata metaPacket = BossHealth.access$1(watcher);
/*     */ 
/* 206 */         BossHealth.access$2(this.val$player, metaPacket);
/*     */       } } );
/*     */   }
/*     */ 
/*     */   public static void removeText(Player player) {
/* 212 */     PacketPlayOutEntityDestroy destroyEntityPacket = getDestroyEntityPacket();
/* 213 */     sendPacket(player, destroyEntityPacket);
/* 214 */     if (hasHealthBar.contains(player))
/* 215 */       hasHealthBar.remove(player);
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Game.BossHealth
 * JD-Core Version:    0.6.0
 */