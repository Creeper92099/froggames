/*     */ package de.Me.Rush.Teams;
/*     */ 
/*     */ import de.Me.Rush.Commands.Nick_Command;
/*     */ import de.Me.Rush.Game.GameManager;
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Color;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.Sound;
/*     */ import org.bukkit.block.Block;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.block.Action;
/*     */ import org.bukkit.event.inventory.InventoryClickEvent;
/*     */ import org.bukkit.event.inventory.InventoryMoveItemEvent;
/*     */ import org.bukkit.event.player.PlayerInteractEvent;
/*     */ import org.bukkit.inventory.Inventory;
/*     */ import org.bukkit.inventory.InventoryView;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ import org.bukkit.inventory.meta.LeatherArmorMeta;
/*     */ import org.kitteh.tag.TagAPI;
/*     */ 
/*     */ public class TeamSelector
/*     */   implements Listener
/*     */ {
/*     */   @EventHandler
/*     */   public void onWoolClick(PlayerInteractEvent e)
/*     */   {
/*  33 */     Player p = e.getPlayer();
/*     */ 
/*  35 */     if (Main.status == GameManager.LOBBY)
/*     */     {
/*  37 */       if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK))
/*     */       {
/*  39 */         if (e.getClickedBlock().getType() == Material.WOOL)
/*     */         {
/*  41 */           if (p.getInventory().getItemInHand().getItemMeta().getDisplayName().contains("Wähle"))
/*     */           {
/*  43 */             if (Bukkit.getOnlinePlayers().length >= Main.minPlayer)
/*     */             {
/*  45 */               Inventory inv = Bukkit.createInventory(null, 9, "§5Wähle dein Team");
/*     */ 
/*  47 */               ItemStack item = new ItemStack(Material.WOOL, 1, 14);
/*  48 */               ItemMeta meta = item.getItemMeta();
/*  49 */               meta.setDisplayName("§c§lRotes Team");
/*  50 */               List r = new ArrayList();
/*  51 */               r.add("§c" + Main.red.size() + "/4");
/*  52 */               for (String red : Main.red) {
/*  53 */                 r.add("§c" + red);
/*     */               }
/*  55 */               if (Main.red.size() == 0) {
/*  56 */                 r.add("§aNoch keine Spieler");
/*     */               }
/*  58 */               meta.setLore(r);
/*  59 */               item.setItemMeta(meta);
/*     */ 
/*  61 */               ItemStack item2 = new ItemStack(Material.WOOL, 1, 7);
/*  62 */               ItemMeta meta2 = item2.getItemMeta();
/*  63 */               meta2.setDisplayName("§7§lRandom Team");
/*  64 */               List ra = new ArrayList();
/*  65 */               meta2.setLore(ra);
/*  66 */               item2.setItemMeta(meta2);
/*     */ 
/*  69 */               ItemStack item1 = new ItemStack(Material.WOOL, 1, 11);
/*  70 */               ItemMeta meta1 = item1.getItemMeta();
/*  71 */               meta1.setDisplayName("§9§lBlaues Team");
/*     */ 
/*  73 */               List b = new ArrayList();
/*  74 */               b.add("§b" + Main.blue.size() + "/4");
/*     */ 
/*  76 */               for (String blue : Main.blue)
/*     */               {
/*  78 */                 b.add("§b" + blue);
/*     */               }
/*  80 */               if (Main.blue.size() == 0) {
/*  81 */                 b.add("§6Noch keine Spieler");
/*     */               }
/*  83 */               meta1.setLore(b);
/*     */ 
/*  85 */               item1.setItemMeta(meta1);
/*     */ 
/*  89 */               inv.setItem(1, item);
/*  90 */               inv.setItem(4, item2);
/*  91 */               inv.setItem(7, item1);
/*     */ 
/*  94 */               p.openInventory(inv);
/*     */             } else {
/*  96 */               p.sendMessage(Main.pr + "Es sind nicht genug Spieler Online um das Team zu wählen.");
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @EventHandler
/*     */   public void ons(InventoryMoveItemEvent e) {
/* 107 */     if (Main.status == GameManager.GAME)
/* 108 */       e.setCancelled(false);
/*     */     else
/* 110 */       e.setCancelled(true);
/*     */   }
/*     */ 
/*     */   @EventHandler
/*     */   public void on(InventoryClickEvent e) {
/* 116 */     Player p = (Player)e.getWhoClicked();
/*     */ 
/* 118 */     if (e.getInventory().getName().contains("Wähle")) {
/* 119 */       e.setCancelled(true);
/*     */ 
/* 121 */       if (Main.red.contains(p.getName())) {
/* 122 */         Main.red.remove(p.getName());
/*     */       }
/* 124 */       if (Main.blue.contains(p.getName()))
/* 125 */         Main.blue.remove(p.getName());
/*     */       LeatherArmorMeta meta1;
/* 128 */       if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Random"))
/*     */       {
/* 130 */         Main.random.add(p.getName());
/* 131 */         if (!Nick_Command.nick.containsKey(p.getName()))
/* 132 */           p.setPlayerListName("§7" + p.getName());
/*     */         else {
/* 134 */           p.setPlayerListName("§7" + (String)Nick_Command.nick.get(p.getName()));
/*     */         }
/* 136 */         p.sendMessage(Main.pr + "Dein Team wird Random gewählt.");
/* 137 */         e.getView().close();
/*     */ 
/* 139 */         ItemStack item2 = new ItemStack(Material.WOOL, 1, 7);
/* 140 */         ItemMeta meta2 = item2.getItemMeta();
/* 141 */         meta2.setDisplayName("§7§lRandom Team");
/* 142 */         List ra = new ArrayList();
/* 143 */         meta2.setLore(ra);
/* 144 */         item2.setItemMeta(meta2);
/*     */ 
/* 146 */         ItemStack item1 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 147 */         meta1 = (LeatherArmorMeta)item1.getItemMeta();
/* 148 */         meta1.setDisplayName("§7§lRandom Team");
/* 149 */         meta1.setColor(Color.GRAY);
/* 150 */         item1.setItemMeta(meta1);
/*     */ 
/* 152 */         p.getInventory().setChestplate(item1);
/*     */ 
/* 154 */         p.getInventory().setItem(0, item2);
/* 155 */         TagAPI.refreshPlayer(p);
/* 156 */         p.playSound(p.getEyeLocation(), Sound.FIREWORK_BLAST, 20.0F, 20.0F);
/*     */       }
/*     */       Object localObject;
/*     */       Player all;
/*     */       String blue;
/* 159 */       if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Blaues"))
/*     */       {
/* 163 */         if ((Main.blue.size() == Main.red.size()) || (Main.red.size() > Main.blue.size()))
/*     */         {
/* 165 */           Team t = new Team(p.getName());
/* 166 */           t.addBlueTeam();
/* 167 */           if (!Nick_Command.nick.containsKey(p.getName()))
/* 168 */             p.setPlayerListName("§b" + p.getName());
/*     */           else {
/* 170 */             p.setPlayerListName("§b" + (String)Nick_Command.nick.get(p.getName()));
/*     */           }
/* 172 */           p.sendMessage(Main.pr + "§6Du hast das §bBlaue§6 Team Betreten.");
/* 173 */           p.playSound(p.getEyeLocation(), Sound.FIREWORK_BLAST, 20.0F, 20.0F);
/* 174 */           e.getView().close();
/*     */ 
/* 176 */           ItemStack item1 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 177 */           LeatherArmorMeta meta1 = (LeatherArmorMeta)item1.getItemMeta();
/* 178 */           meta1.setColor(Color.BLUE);
/* 179 */           meta1.setDisplayName("§b§lBlaues TEAM");
/* 180 */           item1.setItemMeta(meta1);
/*     */ 
/* 182 */           p.getInventory().setChestplate(item1);
/*     */           Player[] arrayOfPlayer;
/* 184 */           localObject = (arrayOfPlayer = Bukkit.getOnlinePlayers()).length; for (meta1 = 0; meta1 < localObject; meta1++) { all = arrayOfPlayer[meta1];
/* 185 */             if (!Main.blue.contains(all.getName()))
/*     */               continue;
/* 187 */             ItemStack item = new ItemStack(Material.WOOL, 1, 11);
/* 188 */             ItemMeta meta = item.getItemMeta();
/* 189 */             meta.setDisplayName("§6Du bist im §bBlauen §6Team");
/* 190 */             List ra = new ArrayList();
/* 191 */             for (Iterator localIterator = Main.blue.iterator(); localIterator.hasNext(); ) { blue = (String)localIterator.next();
/* 192 */               ra.add("§bMitglieder:");
/* 193 */               ra.add("§b" + blue);
/*     */             }
/* 195 */             meta.setLore(ra);
/* 196 */             item.setItemMeta(meta);
/*     */ 
/* 198 */             all.getInventory().setItem(0, item);
/* 199 */             TagAPI.refreshPlayer(p);
/*     */           }
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/* 205 */           p.sendMessage(Main.pr + "Suche dir ein anderes Team. Dieses Team muss nicht ausgeglichen werden.");
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/* 210 */       if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Rotes"))
/*     */       {
/* 212 */         if ((Main.red.size() == Main.blue.size()) || (Main.red.size() < Main.blue.size()))
/*     */         {
/* 214 */           Main.red.add(p.getName());
/* 215 */           if (!Nick_Command.nick.containsKey(p.getName()))
/* 216 */             p.setPlayerListName("§c" + p.getName());
/*     */           else {
/* 218 */             p.setPlayerListName("§c" + (String)Nick_Command.nick.get(p.getName()));
/*     */           }
/* 220 */           p.sendMessage(Main.pr + "§6Du hast das §cRote§6 Team Betreten.");
/* 221 */           p.playSound(p.getEyeLocation(), Sound.FIREWORK_BLAST, 20.0F, 20.0F);
/* 222 */           e.getView().close();
/*     */ 
/* 224 */           ItemStack item1 = new ItemStack(Material.LEATHER_CHESTPLATE);
/* 225 */           LeatherArmorMeta meta1 = (LeatherArmorMeta)item1.getItemMeta();
/* 226 */           meta1.setColor(Color.RED);
/* 227 */           meta1.setDisplayName("§c§lRotes TEAM");
/* 228 */           item1.setItemMeta(meta1);
/*     */ 
/* 230 */           p.getInventory().setChestplate(item1);
/* 231 */           TagAPI.refreshPlayer(p);
/* 232 */           meta1 = (localObject = Bukkit.getOnlinePlayers()).length; for (all = 0; all < meta1; all++) { Player all = localObject[all];
/* 233 */             if (!Main.red.contains(all.getName()))
/*     */               continue;
/* 235 */             ItemStack item = new ItemStack(Material.WOOL, 1, 14);
/* 236 */             ItemMeta meta = item.getItemMeta();
/* 237 */             meta.setDisplayName("§6Du bist im §cRoten §6Team");
/* 238 */             List ra = new ArrayList();
/* 239 */             for (String red : Main.red) {
/* 240 */               ra.add("§cMitglieder : ");
/* 241 */               ra.add("§c" + red);
/*     */             }
/* 243 */             meta.setLore(ra);
/* 244 */             item.setItemMeta(meta);
/*     */ 
/* 246 */             item.setItemMeta(meta);
/*     */ 
/* 248 */             all.getInventory().setItem(0, item);
/*     */           }
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/* 257 */           p.sendMessage(Main.pr + "Suche dir ein anderes Team. Dieses Team muss nicht ausgeglichen werden.");
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Teams.TeamSelector
 * JD-Core Version:    0.6.0
 */