/*    */ package de.Me.Rush.Teams;
/*    */ 
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.List;
/*    */ import java.util.Random;
/*    */ 
/*    */ public class Team
/*    */ {
/*    */   String p;
/*    */ 
/*    */   public Team(String player)
/*    */   {
/* 12 */     this.p = player;
/*    */   }
/*    */   public void addRedTeam() {
/* 15 */     Main.red.add(this.p);
/*    */   }
/*    */ 
/*    */   public void givePassendesTeam() {
/* 19 */     if (Main.blue.size() == Main.red.size())
/*    */     {
/* 21 */       Random ran = new Random();
/* 22 */       int gn = ran.nextInt(2);
/* 23 */       gn++;
/* 24 */       if (gn == 1) {
/* 25 */         Main.red.add(this.p);
/*    */       }
/* 27 */       if (gn == 2) {
/* 28 */         Main.blue.add(this.p);
/*    */       }
/*    */     }
/*    */     else
/*    */     {
/* 33 */       if (Main.blue.size() < Main.red.size()) {
/* 34 */         Main.blue.add(this.p);
/*    */       }
/* 36 */       if (Main.red.size() < Main.blue.size())
/* 37 */         Main.red.add(this.p);
/*    */     }
/*    */   }
/*    */ 
/*    */   public void removeFromRedTeam()
/*    */   {
/* 44 */     Main.red.remove(this.p);
/*    */   }
/*    */   public void removeFromBlueTeam() {
/* 47 */     Main.blue.remove(this.p);
/*    */   }
/*    */   public void addBlueTeam() {
/* 50 */     Main.blue.add(this.p);
/*    */   }
/*    */ 
/*    */   public boolean isInBlueTeam() {
/* 54 */     return Main.blue.contains(this.p);
/*    */   }
/*    */ 
/*    */   public boolean isInRedTeam()
/*    */   {
/* 60 */     return Main.red.contains(this.p);
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Teams.Team
 * JD-Core Version:    0.6.0
 */