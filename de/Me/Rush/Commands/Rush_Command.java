/*     */ package de.Me.Rush.Commands;
/*     */ 
/*     */ import de.Me.Rush.Main.Main;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.util.HashMap;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.World;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ import org.bukkit.configuration.file.YamlConfiguration;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ public class Rush_Command
/*     */   implements CommandExecutor
/*     */ {
/*  19 */   HashMap<String, Location> loc1 = new HashMap();
/*  20 */   HashMap<String, Location> loc2 = new HashMap();
/*     */ 
/*     */   public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
/*     */   {
/*  28 */     Player p = (Player)sender;
/*     */ 
/*  30 */     String world = p.getWorld().getName();
/*  31 */     double x = p.getLocation().getX();
/*  32 */     double y = p.getLocation().getY();
/*  33 */     double z = p.getLocation().getZ();
/*  34 */     double yaw = p.getLocation().getYaw();
/*  35 */     double pitch = p.getLocation().getPitch();
/*     */ 
/*  37 */     File file = new File("plugins/Rush/Location", "loc.yml");
/*  38 */     FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
/*     */ 
/*  40 */     if (p.hasPermission("Rush.use"))
/*     */     {
/*  42 */       if (cmd.getName().equalsIgnoreCase("Rush"))
/*     */       {
/*  44 */         if (args.length == 0)
/*     */         {
/*  46 */           p.sendMessage("§c§l==== §a§lRUSH COMMANDS §c§l====");
/*  47 */           p.sendMessage("§6/Rush start");
/*  48 */           p.sendMessage("§6/Rush setBlueSpawn");
/*  49 */           p.sendMessage("§6/Rush setRedSpawn");
/*  50 */           p.sendMessage("§6/Rush setLobby");
/*  51 */           p.sendMessage("§6/Rush pos1");
/*  52 */           p.sendMessage("§6/Rush pos2");
/*  53 */           p.sendMessage("§6/Rush setRegionNotBuild");
/*     */         }
/*  56 */         else if (args.length == 1) {
/*  57 */           if (args[0].equalsIgnoreCase("pos1")) {
/*  58 */             this.loc1.put(p.getName(), p.getLocation());
/*  59 */             p.sendMessage(Main.pr + "Position 1 gesetzt.");
/*     */           }
/*  61 */           if (args[0].equalsIgnoreCase("pos2")) {
/*  62 */             this.loc2.put(p.getName(), p.getLocation());
/*  63 */             p.sendMessage(Main.pr + "Position 2 gesetzt.");
/*     */           }
/*  65 */           if (args[0].equalsIgnoreCase("setRegionNotBuild"))
/*     */           {
/*  67 */             if ((this.loc1.containsKey(p.getName())) && (this.loc2.containsKey(p.getName())))
/*     */             {
/*  69 */               setLocation("Rush.pos1", (Location)this.loc1.get(p.getName()), file, cfg);
/*  70 */               setLocation("Rush.pos2", (Location)this.loc2.get(p.getName()), file, cfg);
/*     */ 
/*  72 */               p.sendMessage(Main.pr + "Du hast die Region gesetzt.");
/*     */             }
/*     */             else {
/*  75 */               p.sendMessage(Main.pr + "Du hast nicht beide Punkte gesetzt.");
/*     */             }
/*     */           }
/*  78 */           if (args[0].equalsIgnoreCase("start")) {
/*  79 */             p.sendMessage(Main.pr + "Du hast das Spiel gestartet.");
/*  80 */             Main.Lobby = 11;
/*     */           }
/*  82 */           if (args[0].equalsIgnoreCase("setBlueSpawn"))
/*     */           {
/*  85 */             setLocation("Rush.Blue", p.getLocation(), file, cfg);
/*  86 */             p.sendMessage(Main.pr + "§3Du hast den §bBlauen §3Spawn erfolgreich gesetzt.");
/*     */           }
/*     */ 
/*  90 */           if (args[0].equalsIgnoreCase("setRedSpawn"))
/*     */           {
/*  93 */             setLocation("Rush.Red", p.getLocation(), file, cfg);
/*  94 */             p.sendMessage(Main.pr + "§3Du hast den §cRoten §3Spawn erfolgreich gesetzt.");
/*     */           }
/*     */ 
/*  98 */           if (args[0].equalsIgnoreCase("setLobby"))
/*     */           {
/* 100 */             setLocation("Lobby", p.getLocation(), file, cfg);
/*     */ 
/* 102 */             p.sendMessage(Main.pr + "§3Du hast den §dLobby §3Spawn erfolgreich gesetzt.");
/*     */           }
/*     */         }
/*     */         else
/*     */         {
/* 107 */           args.length;
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 120 */       p.sendMessage(Main.pr + "Keine Permissions.");
/*     */     }
/*     */ 
/* 123 */     return false;
/*     */   }
/*     */ 
/*     */   public void setLocation(String path, Location loc, File file, FileConfiguration cfg)
/*     */   {
/* 128 */     cfg.set(path + ".world", loc.getWorld().getName());
/* 129 */     cfg.set(path + ".x", Double.valueOf(loc.getX()));
/* 130 */     cfg.set(path + ".y", Double.valueOf(loc.getY()));
/* 131 */     cfg.set(path + ".z", Double.valueOf(loc.getZ()));
/* 132 */     cfg.set(path + ".yaw", Float.valueOf(loc.getYaw()));
/* 133 */     cfg.set(path + ".pitch", Float.valueOf(loc.getPitch()));
/*     */     try
/*     */     {
/* 136 */       cfg.save(file);
/*     */     }
/*     */     catch (IOException e) {
/* 139 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Commands.Rush_Command
 * JD-Core Version:    0.6.0
 */