/*    */ package de.Me.Rush.Commands;
/*    */ 
/*    */ import de.Me.Rush.Game.GameManager;
/*    */ import de.Me.Rush.Main.Main;
/*    */ import java.util.HashMap;
/*    */ import java.util.List;
/*    */ import org.bukkit.command.Command;
/*    */ import org.bukkit.command.CommandExecutor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;
/*    */ import org.kitteh.tag.TagAPI;
/*    */ 
/*    */ public class Nick_Command
/*    */   implements CommandExecutor, Listener
/*    */ {
/* 20 */   public static HashMap<String, String> nick = new HashMap();
/*    */ 
/*    */   public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
/*    */   {
/* 25 */     Player p = (Player)sender;
/*    */ 
/* 27 */     if (cmd.getName().equalsIgnoreCase("nick"))
/*    */     {
/* 29 */       if (args.length == 0)
/*    */       {
/* 31 */         p.sendMessage("§3/Nick <name>");
/*    */       }
/* 34 */       else if (args.length >= 1)
/*    */       {
/* 36 */         String name = args[0];
/*    */ 
/* 38 */         if (name.length() < 4) {
/* 39 */           p.sendMessage(Main.pr + "Der Name ist zu kurz.");
/* 40 */           return true;
/*    */         }
/* 42 */         if (name.length() >= 17) {
/* 43 */           p.sendMessage(Main.pr + "Der Name ist zu lang.");
/* 44 */           return true;
/*    */         }
/*    */ 
/* 47 */         nick.put(p.getName(), name);
/* 48 */         if (Main.status == GameManager.LOBBY)
/*    */         {
/* 50 */           if (Main.blue.contains(p.getName())) {
/* 51 */             p.setPlayerListName("§b" + name);
/*    */           }
/* 53 */           if (Main.red.contains(p.getName())) {
/* 54 */             p.setPlayerListName("§c" + name);
/*    */           }
/* 56 */           if (Main.random.contains(p.getName()))
/* 57 */             p.setPlayerListName("§7" + name);
/*    */         }
/*    */         else {
/* 60 */           if (Main.blue.contains(p.getName())) {
/* 61 */             p.setPlayerListName("§b" + name);
/*    */           }
/* 63 */           if (Main.red.contains(p.getName())) {
/* 64 */             p.setPlayerListName("§c" + name);
/*    */           }
/*    */         }
/* 67 */         TagAPI.refreshPlayer(p);
/* 68 */         p.sendMessage(Main.pr + "§3Du heißt jetzt §e" + name + "§3.");
/*    */       }
/*    */ 
/*    */     }
/*    */ 
/* 77 */     return true;
/*    */   }
/*    */ 
/*    */   @EventHandler
/*    */   public void on(AsyncPlayerReceiveNameTagEvent e) {
/* 83 */     Player p = e.getPlayer();
/* 84 */     Player s = e.getNamedPlayer();
/*    */ 
/* 86 */     if (nick.containsKey(s.getName()))
/*    */     {
/* 88 */       if (Main.status == GameManager.LOBBY)
/*    */       {
/* 90 */         if (Main.blue.contains(s.getName())) {
/* 91 */           e.setTag("§b" + (String)nick.get(s.getName()));
/* 92 */           return;
/*    */         }
/* 94 */         if (Main.red.contains(s.getName())) {
/* 95 */           e.setTag("§c" + (String)nick.get(s.getName()));
/* 96 */           return;
/*    */         }
/* 98 */         if (Main.random.contains(s.getName())) {
/* 99 */           e.setTag("§7" + (String)nick.get(s.getName()));
/* 100 */           return;
/*    */         }
/* 102 */         e.setTag((String)nick.get(s.getName()));
/*    */       }
/*    */       else
/*    */       {
/* 107 */         if (Main.blue.contains(s.getName())) {
/* 108 */           e.setTag("§b" + (String)nick.get(s.getName()));
/* 109 */           return;
/*    */         }
/* 111 */         if (Main.red.contains(s.getName())) {
/* 112 */           e.setTag("§c" + (String)nick.get(s.getName()));
/* 113 */           return;
/*    */         }
/*    */ 
/*    */       }
/*    */ 
/*    */     }
/* 120 */     else if (Main.status == GameManager.LOBBY)
/*    */     {
/* 122 */       if (Main.blue.contains(s.getName())) {
/* 123 */         e.setTag("§b" + s.getName());
/* 124 */         return;
/*    */       }
/* 126 */       if (Main.red.contains(s.getName())) {
/* 127 */         e.setTag("§c" + s.getName());
/* 128 */         return;
/*    */       }
/* 130 */       if (Main.random.contains(s.getName())) {
/* 131 */         e.setTag("§7" + s.getName());
/* 132 */         return;
/*    */       }
/*    */     } else {
/* 135 */       if (Main.blue.contains(s.getName())) {
/* 136 */         e.setTag("§b" + s.getName());
/* 137 */         return;
/*    */       }
/* 139 */       if (Main.red.contains(s.getName())) {
/* 140 */         e.setTag("§c" + s.getName());
/* 141 */         return;
/*    */       }
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Kirill Neduzhiy\Downloads\sRush.jar
 * Qualified Name:     de.Me.Rush.Commands.Nick_Command
 * JD-Core Version:    0.6.0
 */